<?php

/**
 * @brief Create the DB Query menu and return it.
 */
function _mini_dbquery_menu_array() {
  $items = array();

  $items['admin/settings/dbquery'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'DB Query',
    'description' => 'Define what can be queried.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mini_dbquery_settings'),
    'access arguments' => array('mini dbquery administration'),
    'file' => 'mini_dbquery.admin.inc',
  );

  // "password" (secret path), name of a field, identifier
  $items['dbquery/%/%/%'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'DB Query',
    'description' => 'Define what can be queried.',
    'page callback' => 'mini_dbquery_field',
    'page arguments' => array(1, 2, 3),
    'access arguments' => array('mini dbquery access'),
    'file' => 'mini_dbquery.pages.inc',
  );
  // "password" (secret path), name of a field
  $items['dbquery/%/%'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'DB Query',
    'description' => 'Define what can be queried.',
    'page callback' => 'mini_dbquery_field',
    'page arguments' => array(1, 2),
    'access arguments' => array('mini dbquery access'),
    'file' => 'mini_dbquery.pages.inc',
  );

  return $items;
}

/**
 * @brief Form to setup the DB Query functionality.
 */
function mini_dbquery_settings() {
  $form = array();

  $form['mini_dbquery_warning'] = array(
    '#value' => '<span style="color: red;">' . t('WARNING:') . '</span>'
      . t(' Use this module at your own risk. I very strongly suggest that you do not use it if you don\'t understand it.'),
  );

  $form['mini_dbquery'] = array(
    '#type' => 'fieldset',
    '#title' => t('DB Query settings'),
    '#description' => t('Define a secret path and the tables and fields that can be queried.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['mini_dbquery']['mini_dbquery_secret_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret path'),
    '#default_value' => variable_get('mini_dbquery_secret_path', ''),
    '#description' => t('Enter a secret path so avoid unwanted eyes on your data. If you are creating a public API, then you do not need to enter a complicated path. Otherwise, I suggest a password like entry (well, with just letters and digits, probably all lowercase.) This is used in the path as in: dbquery/&lt;secret path&gt;/field-name'),
  );

  $form['mini_dbquery']['mini_dbquery_fields'] = array(
    '#type' => 'textarea',
    '#title' => t('Authorized fields'),
    '#default_value' => variable_get('mini_dbquery_fields', ''),
    '#description' => t('List of authorized fields, one per line defined as &lt;name&gt; <em>space(s)</em> &lt;table[id].field&gt;. The table[id].field needs to be an existing table and field at the time the field is queried. The id parameter is the name of an identifier. When omited, the entire table is returned (watch out on that one!)'),
    '#rows' => 10,
    '#wysiwyg' => FALSE,
  );

  $form['#validate'][] = 'mini_dbquery_settings_validate';

  return system_settings_form($form);
}

/**
 * @brief Validate the DB Query fields.
 */
function mini_dbquery_settings_validate($form, &$form_state) {
  // We want to make sure that the secret path only includes "valid" (acceptable) characters
  if (!preg_match('/^[0-9A-Za-z_-]+$/', $form_state['values']['mini_dbquery_secret_path'])) {
    form_set_error('mini_dbquery_secret_path', t('The secret path cannot include "weird" characters. Only URL characters are accepted at this point [0-9], [a-z] and the dash (-).'));
  }

  $err = '';
  $fields = _mini_dbquery_parse_fields($form_state['values']['mini_dbquery_fields'], $err);
  if ($fields === FALSE) {
    form_set_error('mini_dbquery_fields', $err);
  }
  else {
    variable_set('mini_dbquery_fields_compiled', $fields);
  }
}

/**
 * @brief Compile the fields.
 */
function _mini_dbquery_parse_fields($fields, &$err) {
  $err = NULL;
  $result = array();
  $fields = str_replace("\r", "\n", $fields);
  $lines = explode("\n", $fields);
  foreach ($lines as $l) {
    $l = trim($l);
    if ($l && $l[0] != '#') {
      $field_info = explode(' ', $l, 2);
      if (count($field_info) != 2) {
        // missing space
        $err = t('@l: does no include a field name and a table/field specification. ()', array('@l' => $l));
        return FALSE;
      }
      if (!preg_match('/^[0-9A-Za-z-]+$/', $field_info[0])) {
        // field name is invalid
        $err = t('@l: uses an incompatible name for the field name. Only [0-9], [A-Z] and the dash (-) are allowed.', array('@l' => $l));
        return FALSE;
      }
      $field_info[1] = str_replace(' ', '', $field_info[1]);
      if (!preg_match('/^([A-Za-z_][0-9A-Za-z_]*)(?:\[([A-Za-z_][0-9A-Za-z_]*)\])?\.([A-Za-z_][0-9A-Za-z_]*)$/', $field_info[1], $m)) {
        // field name is invalid
        $err = t('@l: uses an incompatible table[id].field syntax. Only [0-9], [A-Z] and the underscore (_) are allowed in the names. Only the id is optional.', array('@l' => $l));
        return FALSE;
      }
      $r = array(
        'table' => $m[1],
        'field' => $m[3],
      );
      if ($m[2]) {
        $r['id'] = array($m[2]);  // later we'll suppose multiple IDs separated by commas.
      }
      $result[$field_info[0]] = $r;
    }
  }

  return $result;
}

// vim: ts=2 sw=2 et syntax=php
