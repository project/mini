<?php
// $ Id: $

/**
 * \brief Generate the actual menu.
 */
function _mini_menu_array() {
  $items['admin/mini'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Mini',
    'description' => 'Mini-modules settings.',
    'page callback' => 'mini_settings',
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  $items['admin/mini/rebuild_menu'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Rebuild menus',
    'description' => 'Force the system to rebuild your menus on next access.',
    'page callback' => 'mini_rebuild_menu',
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  $items['admin/mini/rebuild_css_js'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Rebuild CSS & JS',
    'description' => 'Force the system to send a new version of the CSS and JS files.',
    'page callback' => 'mini_rebuild_css_js',
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  $items['admin/mini/rebuild_theme_registry'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Rebuild theme registry',
    'description' => 'Force the system to rebuild the theme registry.',
    'page callback' => 'mini_rebuild_theme_registry',
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  $items['admin/mini/rebuild_schema'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Rebuild schema cache',
    'description' => 'Force the system to rebuild the schema cache.',
    'page callback' => 'mini_rebuild_schema',
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  $items['admin/mini/domain_front_page_redirect'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Domain front page redirect',
    'description' => 'Redirect domains and sub-domains to a specific page instead of the default front page.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mini_front_page_redirect_form'),
    'access arguments' => array('mini administration'),
    'file' => 'mini.admin.inc',
  );

  return $items;
}

/**
 * \brief Generate the list of mini settings.
 */
function mini_settings() {
    $main_menu = menu_get_item('admin/mini');
    $content = system_admin_menu_block((array)$main_menu);
    return theme('admin_block_content', $content);
}

/**
 * \brief Mark the menus for rebuild.
 */
function mini_rebuild_menu() {
  // request a rebuild, this puts the value in the cache as well
  // so we do not need to reset the cache
  variable_set('menu_rebuild_needed', TRUE);
  drupal_goto('admin/mini');
}

/**
 * \brief Clear the CSS/JS cache so it gets rebuilt.
 */
function mini_rebuild_css_js() {
  _drupal_flush_css_js();
  drupal_goto('admin/mini');
}

/**
 * \brief Delete all the theme registries.
 *
 * The theme system will cache the theme information
 * (i.e. results of the hook_theme() functions.) When
 * you change one of those functions, in order to have
 * it function, you need to clear this cache.
 */
function mini_rebuild_theme_registry() {
  cache_clear_all('theme_registry:', 'cache', TRUE);
  drupal_goto('admin/mini');
}

/**
 * \brief Delete the schema cache entry.
 *
 * Drupal core caches the schema of all your modules in a
 * cache with cid 'schema'. This function deletes that
 * cache entry so when you change your schema and database
 * synchroneously without create a new update each time
 * (i.e. without wasting your time as much) you may need
 * to reset the cache. In general, this is necessary if you
 * use the drupal_write_record() function which makes use
 * of that cache.
 */
function mini_rebuild_schema() {
  cache_clear_all('schema', 'cache', TRUE);
  drupal_goto('admin/mini');
}

/**
 * \brief Create the form to edit the front page redirect feature.
 */
function mini_front_page_redirect_form() {
  $form['mini_front_page_redirect'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front page redirect settings'),
    '#description' => t('Define a set of domains and sub-domains and their redirection.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['mini_front_page_redirect']['mini_redirect_info'] = array(
    '#type' => 'textarea',
    '#title' => t('Redirections'),
    '#default_value' => variable_get('mini_redirect_info', ''),
    '#description' => t('<p>Enter one redirection per line. The redirection is a full domain name, a space and the new full URL. Optionally, you can add another space and the redirection code (301 Permanent, 302 Found, 303 See other, 304 Not modified, 305 Use proxy, 307 Temporary redirect.) Regular expressions are supported when the domain name is written between slashes. i.e. <code>/.+\.example\.com/ http://example.com</code> will match any sub-domain and simply remove it. Input replacements are accepted in the new location string. <code>/(.+)\.example\.com/ http://example.com/\1</code>. Only the front page is affected. You are responsible to avoid loops. This is generally used to send people from one sub-domain to the main domain + a path as in:</p><p>blog.example.com www.example.com/journal/blog</p>'),
    '#rows' => 25,
    '#wysiwyg' => FALSE,
  );

  $form['#submit'][] = 'mini_front_page_redirect_form_submit';

  return system_settings_form($form);
}

/**
 * \brief Transform the string from the user in a ready to use array of regex + location.
 */
function mini_front_page_redirect_form_submit($form, &$form_state) {
  $result = array();
  $e = $form_state['values']['mini_redirect_info'];
  $e = str_replace("\r", "\n", $e);
  $lines = explode("\n", $e);
  foreach ($lines as $l) {
    $s = explode(' ', $l);
    $cnt = count($s);
    if ($cnt == 2 || $cnt == 3) {
      $pattern = $s[0];
      $location = $s[1];
      if ($cnt == 3) {
        $redirect = (int)$s[2];
        if ($redirect < 301 || $redirect > 599) {
          $redirect = 302;
        }
      }
      else {
        $redirect = 302;
      }

      // although it should not be there accept the http[s]:// by remove it silently
      if (substr($pattern, 0, 7) == 'http://') {
        $pattern = substr($pattern, 7);
        if (substr($pattern, -1) == '/') {
          $pattern = substr($pattern, 0, -1);
        }
      }
      elseif (substr($pattern, 0, 8) == 'https://') {
        $pattern = substr($pattern, 8);
        if (substr($pattern, -1) == '/') {
          $pattern = substr($pattern, 0, -1);
        }
      }
      if ($pattern[0] != '/' || substr($pattern, -1) != '/') {
        // transform the string in a regex by escaping any special char.
        $pattern = quotemeta($pattern);
        $pattern = str_replace('/', '\\/', $pattern);
        $pattern = '/' . $pattern . '/';
      }

      // breakup the location as a path, query and fragment
      $q = strpos($location, '?');
      if ($q !== FALSE) {
        $path = substr($location, 0, $q);
        $f = strpos($location, '#', $q);
        if ($f !== FALSE) {
          $query = substr($location, $p, $q);
          $fragment = substr($location, $q);
        }
        else {
          $query = substr($location, $p);
          $fragment = '';
        }
      }
      else {
        $query = '';
        $f = strpos($location, '#');
        if ($f !== FALSE) {
          $path = substr($location, 0, $f);
          $fragment = substr($location, $f);
        }
        else {
          $path = $location;
          $fragment = '';
        }
      }

      // save the result (same as calling drupal_goto() function)
      if (isset($result[$pattern])) {
        drupal_set_message('The pattern @pattern is defined twice. Only the first instance is kept.', array('@pattern' => $pattern));
      }
      else {
        $result[$pattern] = array(
          'path' => $path,
          'query' => $query,
          'fragment' => $fragment,
          'http_response_code' => $redirect
        );
      }
    }
    else if ($cnt != 0) {
      drupal_set_message('The line @line is not a valid redirection.', array('@line' => $l));
    }
  }

  variable_set('mini_redirect_info_array', $result);
}

// vim: ts=2 sw=2 et syntax=php
