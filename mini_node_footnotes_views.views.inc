<?php

/**
 * @file
 * To do views declarations.
 */

/**
 * Implementation of hook_views_handlers().
 */
function mini_node_footnotes_views_views_handlers() {
  return array(
    'handlers' => array(
      'mini_node_footnotes_views_handler_field' => array(
        'parent' => 'views_handler_field_markup',
      ),
      'mini_node_footnotes_views_handler_field_body_content' => array(
        'parent' => 'mini_node_footnotes_views_handler_field',
      ),
      'mini_node_footnotes_views_handler_field_body_footnotes' => array(
        'parent' => 'mini_node_footnotes_views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function mini_node_footnotes_views_views_data() {
  $data = array();

  // node view extensions

  $data['node_revisions']['body_content'] = array(
    'group' => t('Node'),
    'title' => t('Body content'),
    'help' => t('The content of the body without the footnotes.'),
    'field' => array(
      'field' => 'body',
      'handler' => 'mini_node_footnotes_views_handler_field_body_content',
      'format' => 'format',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['node_revisions']['body_footnotes'] = array(
    'group' => t('Node'),
    'title' => t('Body footnotes'),
    'help' => t('The footnotes from the entire body.'),
    'field' => array(
      'field' => 'body',
      'handler' => 'mini_node_footnotes_views_handler_field_body_footnotes',
      'format' => 'format',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['node_revisions']['teaser_content'] = array(
    'group' => t('Node'),
    'title' => t('Teaser content'),
    'help' => t('The content of the teaser without the footnotes.'),
    'field' => array(
      'field' => 'teaser',
      'handler' => 'mini_node_footnotes_views_handler_field_body_content',
      'format' => 'format',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['node_revisions']['teaser_footnotes'] = array(
    'group' => t('Node'),
    'title' => t('Teaser footnotes'),
    'help' => t('The footnotes from the teaser.'),
    'field' => array(
      'field' => 'teaser',
      'handler' => 'mini_node_footnotes_views_handler_field_body_footnotes',
      'format' => 'format',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  return $data;
}

// vim: ts=2 sw=2 et syntax=php
