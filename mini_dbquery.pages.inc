<?php

/**
 * @brief Read one field and return it as text to the caller.
 */
function mini_dbquery_field($secret_path, $field_name, $id = NULL) {
  // valid secret?
  $p = variable_get('mini_dbquery_secret_path', '');
  if (empty($p) || empty($secret_path) || $secret_path != $p) {
    drupal_access_denied();
  }

  // get existing fields
  $fields = variable_get('mini_dbquery_fields_compiled', NULL);
  if (empty($fields)) {
    drupal_access_denied();
  }

  // is that field even defined?
  if (!isset($fields[$field_name])) {
    drupal_access_denied();
  }
  $f = $fields[$field_name];

  $params = array();
  $sql = "SELECT " . $f['field'] . " FROM {" . $f['table'] . "}";
  if (!empty($f['id'])) {
    if (empty($id)) {
      // the parameter is required!
      drupal_access_denied();
    }
    // at this time we only expect numeric IDs
    $sql .= " WHERE " . $f['id'][0] . " = %d";
    $params[] = $id;
  }
  else {
    if (!empty($id)) {
      // the parameter is unwanted and should not be defined
      drupal_access_denied();
    }
  }

  // we return everything as plain text
  drupal_set_header('Content-Type: text/plain; charset=utf-8');

  // output the result
  $result = db_query($sql, $params);
  while ($row = db_fetch_array($result)) {
    echo $row[$f['field']], "\n";
  }

  exit(0);
}

// vim: ts=2 sw=2 et syntax=php
